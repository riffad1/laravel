<!DOCTYPE html>
<html>
<head>
    <title>Form Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <b>Sign Up Form</b>
    <br><br>
    <form action="{{url('welcome')}}">
        <label for="first_name">First name:</label>
        <br><br>
        <input type="text" id="first_name" value="">
        <br><br>
        <label for="last_name">Last name:</label>
        <br><br>
        <input type="text" id="last_name" value="">
        <br><br>

        <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="0">Male<br>
        <input type="radio" name="gender" value="1">Female<br>
        <input type="radio" name="gender" value="2">Other<br><br>
        
        <label>Nationality:</label>
        <select name="" id="">
            <option value="malaysia">Indonesia</option>
            <option value="australia">Australia</option>
            <option value="singapura">Singapura</option>
            <option value="malaysia">Malaysia</option>
            <option value="thailand">Thailand</option>
        </select>
        <br><br>

        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="language" value="0"> Bahasa Indonesia <br>
        <input type="checkbox" name="language" value="1"> English <br>
        <input type="checkbox" name="language" value="2"> Other <br>

        <p>Bio:</p>
        <textarea name="" id="" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
        <!-- Another way -->
        <!-- <button type="submit">Sign Up</button> -->
    </form>
    
    
</body>
</html>